FROM ubuntu:18.04
MAINTAINER Steyn Geldenhuys "steyn@truevolve.technology"

USER root
RUN dpkg --add-architecture i386 && \
    apt-get update && \
    apt-get install -y sudo git software-properties-common wine64 libgtk-3-dev gtk2-engines libxtst6 libxxf86vm1 freeglut3 libxslt1.1 \
    libcanberra-gtk-module libxext-dev libxrender-dev libxtst-dev* x11-apps git unzip wget apt-utils python3 curl osslsigncode python3-pip && \
    apt-get clean && apt-get purge && apt-get autoremove && \
    pip3 install pyinstaller

COPY zulu11.31.11-ca-jdk11.0.3-linux_x64.tar.gz /tmp/zulu-11-amd64.tar.gz
RUN mkdir -p /usr/lib/jvm/zulu-11-amd64 && \
    tar xfvz /tmp/zulu-11-amd64.tar.gz --directory /usr/lib/jvm/zulu-11-amd64 --strip-components 1 && \
    sh -c 'for bin in /usr/lib/jvm/zulu-11-amd64/bin/*; do update-alternatives --install /usr/bin/$(basename $bin) $(basename $bin) $bin 100; done' && \
    sh -c 'for bin in /usr/lib/jvm/zulu-11-amd64/bin/*; do update-alternatives --set $(basename $bin) $bin; done'

COPY zulu11.1.8-ca-jdk11-c2-linux_aarch32hf.tar.gz /tmp/zulu-11-aarch32hf.tar.gz
RUN mkdir -p /usr/lib/jvm/zulu-11-aarch32hf && \
    tar xfvz /tmp/zulu-11-aarch32hf.tar.gz --directory /usr/lib/jvm/zulu-11-aarch32hf --strip-components 1


COPY openjfx-11.0.2_linux-x64_bin-sdk.zip /tmp/openjfx-11-amd64.zip
RUN mkdir -p /home/developer/openjfx-11 && \
    unzip -d /home/developer/openjfx-11 /tmp/openjfx-11-amd64.zip

COPY openjfx-11.0.2_armv6hf_bin-sdk.zip /tmp/openjfx-11-amd64.zip
RUN mkdir -p /home/developer/openjfx-11 && \
    unzip -d /home/developer/openjfx-11 /tmp/openjfx-11-amd64.zip

# Add the google cloud sdk. This will allow child containers to push to google cloud repositories
RUN echo "deb http://packages.cloud.google.com/apt cloud-sdk-xenial main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get update -y && apt-get install google-cloud-sdk -y

RUN mkdir -p /home/developer/ && \
    echo "developer:x:1000:1000:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:1000:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    adduser developer video && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown developer:developer -R /home/developer

USER developer

COPY wine64 /home/developer/wine64

ENV GRADLE_FOLDER /home/developer/.gradle
RUN mkdir ${GRADLE_FOLDER}
RUN echo "allprojects {\n    repositories {\n        mavenLocal()\n            maven {\n	        credentials {\n	            username travenUser\n	            password travenPassword\n\
            }\n            url \"https://truevolve-traven.appspot.com\"\n        }\n    }\n}\n" > ${GRADLE_FOLDER}/init.gradle

RUN echo "travenUser=REPLACE_THIS_WITH_YOUR_USERNAME\ntravenPassword=REPLACE_THIS_WITH_YOUR_PASSWORD\norg.gradle.caching=true" > ${GRADLE_FOLDER}/gradle.properties
RUN sudo chmod -R 0777 /home/developer/wine64/ && \
    sudo chown developer:developer -R /home/developer
     
ENV JAVA_HOME /usr/lib/jvm/zulu-11-amd64
ENV HOME /home/developer
WORKDIR /home/developer

RUN wget "https://www.python.org/ftp/python/3.4.1/python-3.4.1.msi" && \
    wine msiexec /i python-3.4.1.msi /quiet ADDLOCAL=ALL TARGETDIR="C:\windows\system32" && \
    wine pip install py2exe==0.9.2.2